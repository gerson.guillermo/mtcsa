import queue
import logging
import time
from logging.handlers import RotatingFileHandler
from logging import handlers
import sys
from src.autentIniOpera.infrastructure.controller import AutentIniOpera
from src.sql.infrastructure.controller import SqlTask
from src.valVehPol.infrastructure.controller import ValVehPol

#SETTINGS LOGGER
logger = logging.getLogger('registers')
logger.setLevel(logging.INFO)
format = logging.Formatter("%(asctime)s; %(levelname)s; %(message)s")
ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(format)
logger.addHandler(ch)
fh = handlers.RotatingFileHandler('registers.log', maxBytes=(10*1024*1024), backupCount=1)
fh.setFormatter(format)
logger.addHandler(fh)

#MAIN
def initState():
    try:
        sqlTask = SqlTask()
        conx = sqlTask.connection()

        while True:
            #METHOD MTC: AUTENTIFICA INICIO OPERACION
            autentIniOpera = AutentIniOpera(conx, logger)
            autentIniOpera.startProcess()

            #METHOD MTC: VAL VEHICULO POLIZA
            valVehPol = ValVehPol(conx, logger)
            valVehPol.startProcess()
            time.sleep(1)

    except Exception as err:
        logger.error(str(err))

        #RE-CONNECT DATABASE
        time.sleep(10)
        initState()

#START
initState()
