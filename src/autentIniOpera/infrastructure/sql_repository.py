from ..domain.receive import Receive


class SqlRepository():
    def __init__(self,conx):
        self.conn = conx

    def collectData(self):
        cursor = self.conn.cursor()
        cursor.execute('SELECT id, P_CODENTIDA, P_CODLOCAL, P_CODIV FROM tb_autentificaInicioOperacion WHERE FLG_PROCESADO=0 AND FLG_ACTIVO=1 ORDER BY id DESC;')
        raw = cursor.fetchall()
        cursor.close()
        return raw
    
    def insertData(self, response: Receive, idx, raw):
        cursor = self.conn.cursor()
        sentencia = "UPDATE tb_autentificaInicioOperacion SET R_CODIGO='"+ response.codigo +"', FLG_PROCESADO="+ str(1) +", R_MENSAJE='"+ response.mensaje +"', R_RAW='"+ raw +"' WHERE ID = "+str(idx)
        cursor.execute(sentencia)
        self.conn.commit()
    
    def setError(self, idx, raw):
        cursor = self.conn.cursor()
        sentencia = "UPDATE tb_autentificaInicioOperacion SET FLG_PROCESADO="+ str(2) +", R_RAW='"+ raw +"' WHERE ID = "+str(idx)
        cursor.execute(sentencia)
        self.conn.commit()