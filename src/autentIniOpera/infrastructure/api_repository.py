import requests

from ..domain.send import Send

class ApiRepository():
    def consultData(self, parsed: Send):
        url="https://wscitv.mtc.gob.pe/WSInterOperabilidadCITV.svc"
        headers = {'content-type': 'text/xml',"SOAPAction": "http://tempuri.org/ICITV_TM_Servicios/AutentificaInicioOperacion"}
        
        body = """<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
            <Body>
                <AutentificaInicioOperacion xmlns="http://tempuri.org/">
                    <!-- Optional -->
                    <entLocalLogin>
                        <CodEntidad xmlns="http://schemas.datacontract.org/2004/07/MTC.ServiciosCITV.Dominio.Entidades">{}</CodEntidad>
                        <CodIV xmlns="http://schemas.datacontract.org/2004/07/MTC.ServiciosCITV.Dominio.Entidades">{}</CodIV>
                        <CodLocal xmlns="http://schemas.datacontract.org/2004/07/MTC.ServiciosCITV.Dominio.Entidades">{}</CodLocal>
                    </entLocalLogin>
                </AutentificaInicioOperacion>
            </Body>
        </Envelope>""".format(parsed.P_CODENTIDA,parsed.P_CODIV,parsed.P_CODLOCAL)

        response = requests.post(url,data=body,headers=headers)
        return response