from ..application.error import ApiErrorRespond, ParsedNotMatch
from ..application.controlResponse import ControlResponse
from .api_repository import ApiRepository
from .sql_repository import SqlRepository

class AutentIniOpera():
    def __init__(self, conx, log):
        self.conn = conx
        self.logger = log
        self.sqlRepository = SqlRepository(conx)
        self.apiRepository = ApiRepository()
        self.controlResponse = ControlResponse()

    def startProcess(self):
        try:
            data = self.sqlRepository.collectData()
            for item in data:
                try:
                    parsed = self.controlResponse.parsedSend(item)
                    raw = self.apiRepository.consultData(parsed)

                    try:
                        response = self.controlResponse.parsedResp(raw)
                        self.sqlRepository.insertData(response, item[0], raw.text)
                    except Exception as err:
                        self.sqlRepository.setError(item[0], raw.text)
                        self.logger.error(err)

                except ApiErrorRespond as err:
                    self.sqlRepository.setError(item[0], "")
                    self.logger.error(err)
                except ParsedNotMatch as err:
                    self.sqlRepository.setError(item[0], "")
                    self.logger.error(err)
                except Exception as err:
                    self.sqlRepository.setError(item[0], str(err))
                    self.logger.error(err)

        except Exception as err:
            self.logger.error(str(err))
            