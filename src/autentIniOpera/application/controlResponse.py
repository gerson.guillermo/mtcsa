from requests.models import Response

from .error import ApiErrorRespond

from ..domain.receive import Receive
from ..domain.send import Send


class ControlResponse():
    def parsedSend(self, raw):
        objX = {
            "P_CODENTIDA" : raw[1],
            "P_CODLOCAL" : raw[2],
            "P_CODIV" : raw[3]
        }
        parsed = Send(**objX)
        return parsed

    def parsedResp(self, raw: Response):
        if raw.status_code == 200:
            parsed = Receive(raw.content)
            return parsed
        else:
            raise ApiErrorRespond("{} - Problema al consumir API AutentificaInicioOperacion".format(str(raw.status_code)))