import json
from ..application.error import ParsedNotMatch

class Send():
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)

    def validation(self, attr, ty, nm):
        try:
            if not isinstance(attr,ty):
                raise Exception('El atributo "'+nm+'" debe establecerse como una instancia '+ str(ty))
        except Exception as err:
            raise err

    def __init__(self, P_CODENTIDA,P_CODLOCAL,P_CODIV):
        try:
            self.validation(P_CODENTIDA, str, 'P_CODENTIDA')
            self.validation(P_CODLOCAL, str, 'P_CODLOCAL')
            self.validation(P_CODIV, str, 'P_CODIV')

            self.P_CODENTIDA = P_CODENTIDA
            self.P_CODLOCAL = P_CODLOCAL
            self.P_CODIV = P_CODIV
        
        except Exception as err:
            raise ParsedNotMatch(err)
        