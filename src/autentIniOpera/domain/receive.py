
import json
import xml.etree.ElementTree as ET
from ..application.error import ParsedNotMatch

class Receive():

    def toJSON(self):
        obj = {
            "mensaje": self.mensaje,
            "codigo": self.codigo
        }
        return json.dumps(obj, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)


    def validation(self, attr, ty, nm):
        try:
            if not isinstance(attr,ty):
                raise Exception('El atributo "'+nm+'" debe establecerse como una instancia '+ str(ty))
        except Exception as err:
            raise err


    def __init__(self, data):
        try:
            parsed = ET.fromstring(data)
            xcodigo = parsed[0][0][0][0].text
            xmensaje = parsed[0][0][0][1].text

            self.validation(xcodigo, (str,type(None)), 'codigo')
            self.validation(xmensaje, (str,type(None)), 'mensaje')

            self.codigo = (parsed[0][0][0][0].text or "")
            self.mensaje = (parsed[0][0][0][1].text or "")
        
        except Exception as err:
            raise ParsedNotMatch(err)
        