from ..application.controlResponse import ControlResponse
from .sqlRepository import SqlRepository
from .apiRepository import ApiRepository

class ValVehPol():
    def __init__(self, conx, log):
        self.conn = conx
        self.logger = log
        self.apiRepository = ApiRepository()
        self.sqlRepository = SqlRepository(conx)
        self.controlResponse = ControlResponse()

    def startProcess(self):
        try:
            raw = self.sqlRepository.collectData()
            for item in raw:
                parsedSend = self.controlResponse.parsedSend(item)
                
                respond = self.apiRepository.sendReg(parsedSend)

                try:
                    parsedReceive = self.controlResponse.parsedReceive(respond)
                    print(parsedReceive.toJSON())
                    # self.sqlRepository.confirm(parsedReceive, respond.text)
                
                except Exception as err:
                    self.logger.error('ValVehPol; ID: '+ item[0] + '; ' +err)
                    # self.sqlRepository.setError(item[0], respond.text)
        
        except Exception as err:
            self.logger.error(str(err))

        

    
