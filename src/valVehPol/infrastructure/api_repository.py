import requests

from ..domain.send import Send

class ApiRepository():
    def sendReg(self, parsed: Send):
        url="https://wscitv.mtc.gob.pe/WSInterOperabilidadCITV.svc"
        headers = {'content-type': 'text/xml',"SOAPAction": "http://tempuri.org/ICITV_TM_Servicios/valVehiculoPoliza"}
        
        body = """<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
    <Body>
        <valVehiculoPoliza xmlns="http://tempuri.org/">
            <!-- Optional -->
            <entVehiculoInspeccion>
                <CATEGORIA xmlns="http://schemas.datacontract.org/2004/07/MTC.ServiciosCITV.Dominio.Entidades">{}</CATEGORIA>
                <CIOD_CITV xmlns="http://schemas.datacontract.org/2004/07/MTC.ServiciosCITV.Dominio.Entidades">{}</CIOD_CITV>
                <PLACA xmlns="http://schemas.datacontract.org/2004/07/MTC.ServiciosCITV.Dominio.Entidades">{}</PLACA>
                <TIPAMBITO xmlns="http://schemas.datacontract.org/2004/07/MTC.ServiciosCITV.Dominio.Entidades">{}</TIPAMBITO>
                <TIPINSPECCION xmlns="http://schemas.datacontract.org/2004/07/MTC.ServiciosCITV.Dominio.Entidades">{}</TIPINSPECCION>
                <TIPSERVICIO xmlns="http://schemas.datacontract.org/2004/07/MTC.ServiciosCITV.Dominio.Entidades">{}</TIPSERVICIO>
            </entVehiculoInspeccion>
        </valVehiculoPoliza>
    </Body>
</Envelope>""".format(parsed.P_CATEGORIA,parsed.P_CIOD_CITV,parsed.P_PLACA,parsed.P_TIPAMBITO,parsed.P_TIPINSPECCION,parsed.P_TIPSERVICIO)

        response = requests.post(url,data=body,headers=headers)
        return response
        