import json

from ..application.error import ParsedNotMatch

class Send():
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)

    def validation(self, attr, ty, nm):
        try:
            if not isinstance(attr,ty):
                raise Exception('El atributo "'+nm+'" debe establecerse como una instancia '+ str(ty))
        except Exception as err:
            raise err

    def __init__(self,P_CIOD_CITV,P_PLACA,P_CATEGORIA,P_TIPSERVICIO,P_TIPAMBITO,P_TIPINSPECCION):
        self.validation(P_CIOD_CITV,(str, type(None)), 'P_CIOD_CITV')
        self.validation(P_PLACA,(str, type(None)), 'P_PLACA')
        self.validation(P_CATEGORIA,(int), 'P_CATEGORIA')
        self.validation(P_TIPSERVICIO,(int), 'P_TIPSERVICIO')
        self.validation(P_TIPAMBITO,(int), 'P_TIPAMBITO')
        self.validation(P_TIPINSPECCION,(int), 'P_TIPINSPECCION')

        try:
            self.P_CIOD_CITV = (P_CIOD_CITV or "")
            self.P_PLACA = (P_PLACA or "")
            self.P_CATEGORIA = P_CATEGORIA
            self.P_TIPSERVICIO = P_TIPSERVICIO
            self.P_TIPAMBITO = P_TIPAMBITO
            self.P_TIPINSPECCION = P_TIPINSPECCION
            
        except Exception as err:
            raise ParsedNotMatch(err)