
import json
import xml.etree.ElementTree as ET
from ..application.error import ParsedNotMatch

class Receive():

    def toJSON(self):
        obj = {
            "xalto" : self.xalto,
            "xancho" : self.xancho,
            "xaniofab" : self.xaniofab,
            "xcarroceria" : self.xcarroceria,
            "xcategoria" : self.xcategoria,
            "xcolor" : self.xcolor,
            "xcombustible" : self.xcombustible,
            "xfecDoc_ultRev" : self.xfecDoc_ultRev,
            "xfecFinPoliza" : self.xfecFinPoliza,
            "xfecIniPoliza" : self.xfecIniPoliza,
            "xlargo" : self.xlargo,
            "xmarca" : self.xmarca,
            "xmensaje" : self.xmensaje,
            "xmodelo" : self.xmodelo,
            "xnumDoc_ultRev" : self.xnumDoc_ultRev,
            "xnumPoliza" : self.xnumPoliza,
            "xnum_Ficha" : self.xnum_Ficha,
            "xnumeroAsientos" : self.xnumeroAsientos,
            "xnumeroEjes" : self.xnumeroEjes,
            "xnumeroMotor" : self.xnumeroMotor,
            "xnumeroPasajeros" : self.xnumeroPasajeros,
            "xnumeroRuedas" : self.xnumeroRuedas,
            "xobs_ultRev" : self.xobs_ultRev,
            "xpesoBruto" : self.xpesoBruto,
            "xpesoNeto" : self.xpesoNeto,
            "xpesoUtil" : self.xpesoUtil,
            "xplaca" : self.xplaca,
            "xrazSocCitv_ultRev" : self.xrazSocCitv_ultRev,
            "xtpPoliza" : self.xtpPoliza,
            "xVINSerCha" : self.xVINSerCha,
        }
        
        return json.dumps(obj, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)


    def validation(self, attr, ty, nm):
        try:
            if not isinstance(attr,ty):
                raise Exception('El atributo "'+nm+'" debe establecerse como una instancia '+ str(ty))
        except Exception as err:
            raise err


    def __init__(self, data):
        try:
            parsed = ET.fromstring(data)

            xalto = parsed[0][0][0][0].text
            xancho = parsed[0][0][0][1].text
            xaniofab = parsed[0][0][0][2].text
            xcarroceria = parsed[0][0][0][3].text
            xcategoria = parsed[0][0][0][4].text
            xcolor = parsed[0][0][0][5].text
            xcombustible = parsed[0][0][0][6].text
            xfecDoc_ultRev = parsed[0][0][0][7].text
            xfecFinPoliza = parsed[0][0][0][8].text
            xfecIniPoliza = parsed[0][0][0][9].text
            xlargo = parsed[0][0][0][10].text
            xmarca = parsed[0][0][0][11].text
            xmensaje = parsed[0][0][0][12].text
            xmodelo = parsed[0][0][0][13].text
            xnumDoc_ultRev = parsed[0][0][0][14].text
            xnumPoliza = parsed[0][0][0][15].text
            xnum_Ficha = parsed[0][0][0][16].text
            xnumeroAsientos = parsed[0][0][0][17].text
            xnumeroEjes = parsed[0][0][0][18].text
            xnumeroMotor = parsed[0][0][0][19].text
            xnumeroPasajeros = parsed[0][0][0][20].text
            xnumeroRuedas = parsed[0][0][0][21].text
            xobs_ultRev = parsed[0][0][0][22].text
            xpesoBruto = parsed[0][0][0][23].text
            xpesoNeto = parsed[0][0][0][24].text
            xpesoUtil = parsed[0][0][0][25].text
            xplaca = parsed[0][0][0][26].text
            xrazSocCitv_ultRev = parsed[0][0][0][27].text
            xtpPoliza = parsed[0][0][0][28].text
            xVINSerCha = parsed[0][0][0][29].text

            

            self.validation(xalto, (str, type(None)), 'xalto')
            self.validation(xancho, (str, type(None)), 'xancho')
            self.validation(xaniofab, (str, type(None)), 'xaniofab')
            self.validation(xcarroceria, (str, type(None)), 'xcarroceria')
            self.validation(xcategoria, (str, type(None)), 'xcategoria')
            self.validation(xcolor, (str, type(None)), 'xcolor')
            self.validation(xcombustible, (str, type(None)), 'xcombustible')
            self.validation(xfecDoc_ultRev, (str, type(None)), 'xfecDoc_ultRev')
            self.validation(xfecFinPoliza, (str, type(None)), 'xfecDoc_ultRev')
            self.validation(xfecIniPoliza, (str, type(None)), 'xfecIniPoliza')
            self.validation(xlargo, (str, type(None)), 'xlargo')
            self.validation(xmarca, (str, type(None)), 'xmarca')
            self.validation(xmensaje, (str, type(None)), 'xmensaje')
            self.validation(xmodelo, (str, type(None)), 'xmodelo')
            self.validation(xnumDoc_ultRev, (str, type(None)), 'xnumDoc_ultRev')
            self.validation(xnumPoliza, (str, type(None)), 'xnumPoliza')
            self.validation(xnum_Ficha, (str, type(None)), 'xnum_Ficha')
            self.validation(xnumeroAsientos, (str, type(None)), 'xnumeroAsientos')
            self.validation(xnumeroEjes, (str, type(None)), 'xnumeroEjes')
            self.validation(xnumeroMotor, (str, type(None)), 'xnumeroMotor')
            self.validation(xnumeroPasajeros, (str, type(None)), 'xnumeroPasajeros')
            self.validation(xnumeroRuedas, (str, type(None)), 'xnumeroRuedas')
            self.validation(xobs_ultRev, (str, type(None)), 'xobs_ultRev')
            self.validation(xpesoBruto, (str, type(None)), 'xpesoBruto')
            self.validation(xpesoNeto, (str, type(None)), 'xpesoNeto')
            self.validation(xpesoUtil, (str, type(None)), 'xpesoUtil')
            self.validation(xplaca, (str, type(None)), 'xplaca')
            self.validation(xrazSocCitv_ultRev, (str, type(None)), 'xrazSocCitv_ultRev')
            self.validation(xtpPoliza, (str, type(None)), 'xtpPoliza')
            self.validation(xVINSerCha, (str, type(None)), 'xVINSerCha')

            self.xalto = (xalto or "")
            self.xancho = (xancho or "")
            self.xaniofab = (xaniofab or "")
            self.xcarroceria = (xcarroceria or "")
            self.xcategoria = (xcategoria or "")
            self.xcolor = (xcolor or "")
            self.xcombustible = (xcombustible or "")
            self.xfecDoc_ultRev = (xfecDoc_ultRev or "")
            self.xfecFinPoliza = (xfecFinPoliza or "")
            self.xfecIniPoliza = (xfecIniPoliza or "")
            self.xlargo = (xlargo or "")
            self.xmarca = (xmarca or "")
            self.xmensaje = (xmensaje or "")
            self.xmodelo = (xmodelo or "")
            self.xnumDoc_ultRev = (xnumDoc_ultRev or "")
            self.xnumPoliza = (xnumPoliza or "")
            self.xnum_Ficha = (xnum_Ficha or "")
            self.xnumeroAsientos = (xnumeroAsientos or "")
            self.xnumeroEjes = (xnumeroEjes or "")
            self.xnumeroMotor = (xnumeroMotor or "")
            self.xnumeroPasajeros = (xnumeroPasajeros or "")
            self.xnumeroRuedas = (xnumeroRuedas or "")
            self.xobs_ultRev = (xobs_ultRev or "")
            self.xpesoBruto = (xpesoBruto or "")
            self.xpesoNeto = (xpesoNeto or "")
            self.xpesoUtil = (xpesoUtil or "")
            self.xplaca = (xplaca or "")
            self.xrazSocCitv_ultRev = (xrazSocCitv_ultRev or "")
            self.xtpPoliza = (xtpPoliza or "")
            self.xVINSerCha = (xVINSerCha or "")
        
        except Exception as err:
            raise ParsedNotMatch(err)
        