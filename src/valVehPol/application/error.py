class ParsedNotMatch(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message =""
    
    def __str__(self):
        if self.message:
            return 'ParsedNotMatch: {0}'.format(self.message)
        else:
            return 'JSON no compatible con el parseo'


class ApiErrorRespond(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message =""
    
    def __str__(self):
        if self.message:
            return 'ApiErrorRespond: {0}'.format(self.message)
        else:
            return 'Problemas al consumir el API'