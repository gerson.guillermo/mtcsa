from .error import ApiErrorRespond
from ..domain.receive import Receive
from ..domain.send import Send

class ControlResponse():
    def parsedSend(self, raw):
        objX = {
            "P_CIOD_CITV" : raw[1],
            "P_PLACA" : raw[2],
            "P_CATEGORIA" : raw[3],
            "P_TIPSERVICIO" : raw[4],
            "P_TIPAMBITO" : raw[5],
            "P_TIPINSPECCION" : raw[6]
        }
        parsed = Send(**objX)
        return parsed

    def parsedReceive(self, raw):
        if raw.status_code == 200:
            parsed = Receive(raw.text)
            return parsed
        else:
            raise ApiErrorRespond("{} - Problema al consumir API AutentificaInicioOperacion".format(str(raw.status_code)))