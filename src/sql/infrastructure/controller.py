import pypyodbc
import configparser


class SqlTask():
    def __init__(self):
        try:
            config = configparser.ConfigParser()
            config.read('./config.ini')
            serverc=config['CONNECTION']['SERVER']
            userc=config['CONNECTION']['USER']
            passc=config['CONNECTION']['PASS']
            dbc=config['CONNECTION']['DATABASE']
            self.conn = pypyodbc.connect('Driver={SQL Server};Server='+serverc+';UID='+userc+';PWD='+passc+';DATABASE='+dbc)

        except Exception as err:
            raise Exception("No se pudo conectar a la base de datos.")
    
    def connection(self):
        return self.conn
        